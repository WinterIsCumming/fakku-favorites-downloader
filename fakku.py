#!/usr/bin/env python3

"""
Download your fakku.net favorites from an exported favorites file.
"""

"""
Author: Winter IsCummin
Repo: https://WinterIsCummin@bitbucket.org/WinterIsCummin/fakku-favorites-downloader.git
Based off darkfeline's fakku_downloader from https://github.com/darkfeline/fakku_downloader
"""

import urllib.request
import urllib.parse
import os
import os.path
import logging
import sys
import json

logger = logging.getLogger(__name__)

def save(url, path):
    logger.debug('save(%r, %r)', url, path)
    if os.path.exists(path):
        raise FileExistsError('File exists.')
    url = urllib.parse.urlsplit(url)
    url = list(url)
    url[2] = urllib.parse.quote(url[2])
    url = urllib.parse.urlunsplit(url)
    requ = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    conn = urllib.request.urlopen(requ)
    data = conn.read()
    conn.close()
    with open(path, 'wb') as f:
        f.write(data)

    
def process_favorites_file(filepath, directory=None, max_tries=3):
    with open(filepath) as favorites_file:
        
        fails_data = {'pages': [], 'titles': {}}
      
      
        favorites = json.load(favorites_file)
        f = 0
        total = len(favorites)
        for favorite in favorites:
            f += 1
            logging.info('Downloading "%s" (%s of %s)' % (favorite['title'].encode("utf-8"), f, total))
            
            apiurl = favorite['api']
            try:
                logging.info('  Retrieving info from API')
                requ = urllib.request.Request(apiurl + '/read', headers={'User-Agent': 'Mozilla/5.0'})
                conn = urllib.request.urlopen(requ)
                rawdata = conn.read().decode('utf-8')
                conn.close()
                
                logging.info('  Parsing API Response')
                
                data = json.loads(rawdata)
            except:
                logging.error('  Failed to call API; Skipping\n    API URL: %s' % apiurl)
                fails_data['titles'][favorite['title']] = 'Failed to call %s' % apiurl
                continue
            
            try:
                series = ' + '.join([series['attribute'] for series in data['content']['content_series']])
                pages = data['content']['content_pages']
                name = data['content']['content_name']
            except:
                logging.error('  Failed to parse API response; Skipping!\n    Response: %s' % data)
                fails_data['titles'][favorite['title']] = 'Required data not found in API response %s' % data
                continue
            
            keepcharacters = (' ','_')
            safe_name = "".join(c for c in name if c.isalnum() or c in keepcharacters).rstrip()
            safe_series = "".join(c for c in series if c.isalnum() or c in keepcharacters).rstrip()
             
            
            logging.info('  Building path')
            path = os.path.join(directory or '.', safe_series, safe_name)
            os.makedirs(path, exist_ok=True)
            
            logging.info('  Saving info file')
            info_path = os.path.join(path, 'info.json')
            with open(info_path, 'w+') as info_file:
                json.dump(data, info_file, indent=4, separators=(',',':'), sort_keys=True)
            
            logging.info('  Downloading %s pages to %s...' % (pages, path))
            
            for i in range(1, pages + 1):
              page = data['pages'][str(i)]
              page_url = page['image']
              page_path = urllib.parse.urlsplit(page_url).path
              page_name = os.path.basename(page_path)
            
              success = False
              for j in range(max_tries):
                  try:
                      save(page_url, os.path.join(path, page_name))
                  except urllib.error.HTTPError as e:
                      logging.error('    HTTP Error details: ', e)
                      continue
                  except FileExistsError:
                      logging.info('    Page {} exists; skipping'.format(i))
                      success = True
                      break
                  else:
                      logging.info('    Page {} complete'.format(i))
                      success = True
                      break
              if not success:
                  logging.info('    Failed to download %s' % page_url)
                  fails_data['pages'].append(page_url)
                  
            logging.info('  Done')

        logging.info('Saving fails report...')
        fails_path = os.path.join(directory, 'Failure_Report.txt')
        with open(fails_path, 'w+') as fails_file:
            json.dump(fails_data, fails_file, indent=4, separators=(',',':'), sort_keys=True)
        
        logging.info('Script complete with %s favorites processed, %s titles skipped, and %s pages skipped.' % (f, len(fails_data['titles']), len(fails_data['pages'])))
        logging.info('Check %s for what was skipped and why.' % fails_path)

def main(*args):

    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('favorites', help='Locations of Fakku exported favorites json file')
    parser.add_argument('directory', nargs='?', default='', help = 'Output directory')
    parser.add_argument('-r', '--retries', type=int, default=3, help='Max number of download attempts per page')
    args = parser.parse_args(args)

    if args.directory: os.makedirs(args.directory, exist_ok=True)
    
    log_path = os.path.join(args.directory or '.', 'log.txt')
    if os.path.exists(log_path): os.remove(log_path)
    logging.basicConfig(filename=log_path, level='INFO')
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    process_favorites_file(args.favorites, args.directory, args.retries)
    


if __name__ == "__main__":
    main(*sys.argv[1:])
