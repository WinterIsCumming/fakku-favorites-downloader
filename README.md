Fakku Favorites Downloader

https://bitbucket.org/WinterIsCummin/fakku-favorites-downloader

Author: Winter Is Cumming

Repo: https://WinterIsCummin@bitbucket.org/WinterIsCummin/fakku-favorites-downloader.git

Based off darkfeline's fakku downloader from https://github.com/darkfeline/fakku_downloader

Instructions
============

1. Install Python 3
2. Place fakku.py in a easy to acces folder
3. In your Fakku profile go to Favorites and click Export Collection and place the downloaded json file in the same directory as fakku.py
4. On the following command to process your favorites and place them in the output subdirectory of the current directory:

    python fakku.py Favorites.json output

  
Usage
=====

    fakku.py [-h] [-r RETRIES] favorites [directory]
    
    positional arguments:
      favorites             Locations of Fakku exported favorites json file
      directory             Output directory
      
    optional arguments:
      -h, --help            show this help message and exit
      -r RETRIES, --retries RETRIES
                            Max number of download attempts per page